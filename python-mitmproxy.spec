%global _empty_manifest_terminate_build 0
Name:                python-mitmproxy
Version:             9.0.1
Release:             2
Summary:             An interactive, SSL/TLS-capable intercepting proxy for HTTP/1, HTTP/2, and WebSockets.
License:             MIT
URL:                 https://github.com/mitmproxy/mitmproxy/
Source0:             https://github.com/mitmproxy/mitmproxy/archive/%{version}/mitmproxy-%{version}.tar.gz
Patch0001:           0001-change-requires-version.patch
BuildArch:           noarch
Requires:            python3-asgiref python3-blinker python3-Brotli python3-certifi python3-click
Requires:            python3-cryptography python3-flask python3-h11 python3-h2 python3-hyperframe
Requires:            python3-kaitaistruct python3-ldap3 python3-msgpack python3-passlib
Requires:            python3-protobuf python3-pyOpenSSL python3-pyparsing python3-pyperclip
Requires:            python3-sortedcontainers python3-tornado python3-urwid python3-wsproto
Requires:            python3-publicsuffix2 python3-zstandard python3-pydivert python3-hypothesis
Requires:            python3-parver python3-pdoc python3-pyinstaller python3-pytest-asyncio
Requires:            python3-pytest-cov python3-pytest-timeout python3-pytest-xdist python3-pytest
Requires:            python3-requests python3-tox python3-wheel
%description
mitmproxy is an interactive, SSL/TLS-capable intercepting proxy with a console interface for HTTP/1, HTTP/2, and WebSockets.

%package -n python3-mitmproxy
Summary:             An interactive, SSL/TLS-capable intercepting proxy for HTTP/1, HTTP/2, and WebSockets.
Provides:            python-mitmproxy
BuildRequires:       python3-devel python3-setuptools
%description -n python3-mitmproxy
mitmproxy is an interactive, SSL/TLS-capable intercepting proxy with a console interface for HTTP/1, HTTP/2, and WebSockets.

%package help
Summary:             Development documents and examples for mitmproxy
Provides:            python3-mitmproxy-doc
%description help
mitmproxy is an interactive, SSL/TLS-capable intercepting proxy with a console interface for HTTP/1, HTTP/2, and WebSockets.

%prep
%autosetup -n mitmproxy-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-mitmproxy -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Feb 11 2025 Ge Wang <wang__ge@126.com> - 9.0.1-2
- Rebuild for branch openEuler-25.03

* Tue Sep 19 2023 xu_ping <707078654@qq.com> - 9.0.1-1
- Upgrade version to 9.0.1
- add Raw UDP and DTLS support.
- Deprecate add_log event hook and mitmproxy.ctx.log.

* Fri Sep 1 2023 liyanan <thistleslyn@163.com> - 7.0.0-6
- Modify the installation dependency protobuf version limit 

* Tue Mar 28 2023 liyanan <liyanan32@h-partners.com> - 7.0.0-5
- change requires version to fix install issue

* Fri Oct 15 2021 chemingdao <chemingdao@huawei.com> - 7.0.0-4
- fix asynocio utils run in python3.7

* Thu Sep 30 2021 chemingdao <chemingdao@huawei.com> - 7.0.0-3
- fix run in python 3.7 environment

* Sat Aug 21 2021 liyanan <liyanan32@huawei.com> - 7.0.0-2
- fix build fail with python3.7

* Fri Jul 30 2021 liyanan <liyanan32@huawei.com> - 7.0.0-1
- package init
